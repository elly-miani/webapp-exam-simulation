package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.*;

import java.io.*;

// Represents the data about a Product
public class Product extends Resource 
{

	// The unique identifier of the Product
	private final int id;

	// The category of the Product
	private final String category;

	// The name of the Product
	private final String name;

	// The age range of the Product
	private final String ageRange;


	// The price of the Product
	private final double price;



	// Creates a new Product
	public Product(final int id, final String category, final String name, final String ageRange, final double price) 
	{
		this.id = id;
		this.category = category;
		this.name = name;
		this.ageRange = ageRange;
		this.price = price;
	}


	// Returns the identifier of the Product
	public final int getIdentifier() 
	{
		return id;
	}

	// Returns the category of the Product
	public final String getCategory() 
	{
		return category;
	}

	// Returns the name of the Product
	public final String getName() 
	{
		return name;
	}

	// Returns the ageRange of the Product
	public final String getAgeRange() 
	{
		return ageRange;
	}

	// Returns the price of the Product
	public final double getPrice() 
	{
		return price;
	}



	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("product");

		jg.writeStartObject();

		jg.writeNumberField("id", id);

		jg.writeStringField("category", category);
		
		jg.writeStringField("name", name);

		jg.writeStringField("ageRange", ageRange);

		jg.writeNumberField("price", price);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}



	// Creates a Product from its JSON representation.
	public static Product fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int id = -1;
		String category = null;
		String name = null;
		String ageRange = null;
		double price = -1;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "product".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no Product object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "id":
						jp.nextToken();
						id = jp.getIntValue();
						break;
					case "category":
						jp.nextToken();
						category = jp.getText();
						break;
					case "name":
						jp.nextToken();
						name = jp.getText();
						break;
					case "ageRange":
						jp.nextToken();
						ageRange = jp.getText();
						break;
					case "price":
						jp.nextToken();
						price = jp.getDoubleValue();
						break;
				}
			}
		}

		return new Product(id, category, name, ageRange, price);
	}
}

