const searchForm = document.getElementById("form-searchByCategory");
var searchCategory; 

const tableResults = document.getElementById("table-search-results");
const tableResultsBody = tableResults.getElementsByTagName("tbody")[0];
const errorMessage = document.getElementById("error-message");


function ajax_get(url, successHandler, errorHandler) {

  var httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		alert('Cannot create an XMLHttpRequest instance.');
		return false;
	}

  httpRequest.onreadystatechange = function() {

		if (httpRequest.readyState === XMLHttpRequest.DONE) {
		
			if (httpRequest.status == 200) {
				// 200 status
				var jsonData = JSON.parse(httpRequest.responseText);
				successHandler(jsonData);
			}
			else {
				// error: pass status code to errorHandler function 
				var jsonData = JSON.parse(httpRequest.responseText);
				errorHandler(jsonData, httpRequest.status);
			}
		}

  };
 
	httpRequest.open('GET', url);
	httpRequest.send();
}




searchForm.addEventListener("submit", function(event) {

	event.preventDefault();
	console.log("Submit button works!");

	
	searchCategoryName = document.getElementById("form-select-searchByCategory").name;
	searchCategoryValue = document.getElementById("form-select-searchByCategory").value;

	//var tempurl = 'http://localhost:8081/mytoys-server-1.00/search-product-by-category' + '?' + searchCategoryName + '=' + searchCategoryValue;
	url = searchForm.action + '?' + searchCategoryName + '=' + searchCategoryValue;
	console.log(url);

	// clean up results table and error messages
	tableResultsBody.innerHTML="";
	errorMessage.innerHTML="";


	ajax_get(url, 
		
		function(jsonData) {
			// 200 status

			jsonData = jsonData['resource-list'];

			// for each product
			for (var i=0; i<jsonData.length; i++) {
				
				product = jsonData[i].product;
		
				var tr = document.createElement('tr');
	
				// for each attribute
				['id','category','name','ageRange','price'].forEach(function(attribute){
					
					var td = document.createElement('td');
					td.appendChild(document.createTextNode(product[attribute]));
					tr.appendChild(td);
				});

				tableResultsBody.appendChild(tr);
			}

			tableResults.classList.remove('hidden');
		},

		function(jsonData, errorCode) {

			if (errorCode == 500) {
				// 500 status

				jsonData = jsonData['message'];

				var ul = document.createElement('ul');

				var li = document.createElement('li');
				li.appendChild(document.createTextNode('Message:' + jsonData['message']));
				ul.appendChild(li);

				li = document.createElement('div');
				li.appendChild(document.createTextNode('Error code:' + jsonData['error-code']));
				ul.appendChild(li);
				
				li = document.createElement('div');
				li.appendChild(document.createTextNode('Error details:' + jsonData['error-details']));
				ul.appendChild(li);


				errorMessage.appendChild(ul);
				errorMessage.classList.remove('hidden');
			}
			else {
				// generic error

				var p = document.createElement('p');

				p.appendChild(document.createTextNode('Unexpected Error'));

				errorMessage.appendChild(p);
				errorMessage.classList.remove('hidden');
			}
				
		}
	)
});
